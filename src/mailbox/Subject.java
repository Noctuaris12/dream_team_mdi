package mailbox;

import java.util.LinkedList;
import java.util.List;

public abstract class Subject {

    private List<Observer> _l;

    public Subject() {
        _l = new LinkedList<>();
    }

    public void attach(Observer o) {
        _l.add(o);
    }

    public void detach(Observer o) {
        _l.remove(o);
    }

    public void notifyObservers() {
        for (Observer o : _l) {
            o.update(this);
        }
    }

}
