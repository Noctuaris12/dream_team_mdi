package mailbox;

import javax.swing.*;

public class CounterObserver extends JLabel implements Observer {

    public CounterObserver() {
        setText("0");
    }

    @Override
    public void update(Subject o) {
        if (o instanceof MailBox) {
            setText(String.valueOf(((MailBox) o).getNbreMail()));
        }
    }

}
