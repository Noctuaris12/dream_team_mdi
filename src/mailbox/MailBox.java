package mailbox;

import java.util.ArrayList;
import java.util.List;



public class MailBox extends Subject {

	private List<Mail> _mail = new ArrayList<Mail>();

	private MailBox(){
		super();
	}

	private static MailBox instance = null;

	public static MailBox getInstance() {
		if (instance==null)
			instance = new MailBox();
		return instance;
	}

	public void addMail(Mail m ){
		_mail.add(m);
		notifyObservers();
	}


	public Mail getLastMail() {
		return _mail.get(_mail.size()-1);
	}


	public Integer getNbreMail() {
		return _mail.size();
	}

}
