package mailbox;

import javax.swing.*;

public class MailObserver extends JLabel implements Observer {

    public MailObserver() {
        setText("No mail !");
    }

    @Override
    public void update(Subject o) {
        if (o instanceof MailBox) {
            setText(((MailBox) o).getLastMail().getSubject());
        }
    }

}
